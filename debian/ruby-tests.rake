require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new(:spec) do |spec|
# https://bugs.debian.org/1015310
# https://jira.mariadb.org/browse/MDEV-28751
if ENV['AUTOPKGTEST_TMP']
  spec.pattern      = './spec/*_spec.rb'
else
  spec.pattern      = 'none'
end
end

task :default => :spec
